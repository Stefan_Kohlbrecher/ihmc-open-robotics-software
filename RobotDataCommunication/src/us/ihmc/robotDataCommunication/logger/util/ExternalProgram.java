package us.ihmc.robotDataCommunication.logger.util;

public interface ExternalProgram
{
   public String getCommandLine();
}

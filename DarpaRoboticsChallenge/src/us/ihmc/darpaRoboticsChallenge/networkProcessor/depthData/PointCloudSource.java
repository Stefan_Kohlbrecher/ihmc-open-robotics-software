package us.ihmc.darpaRoboticsChallenge.networkProcessor.depthData;

public enum PointCloudSource
{
   NEARSCAN,
   QUADTREE,
   LOCALIZER
}

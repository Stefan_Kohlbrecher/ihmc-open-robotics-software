package us.ihmc.quadrupedRobotics.parameters;

import javax.vecmath.Vector3d;

public interface QuadrupedPhysicalProperties
{
   public Vector3d getOffsetFromKneeToFoot();
}

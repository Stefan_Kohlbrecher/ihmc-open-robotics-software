package us.ihmc.quadrupedRobotics.footstepChooser;

public interface QuadrupedControllerParameters extends SwingTargetGeneratorParameters
{

   public double getInitalCoMHeight();

}